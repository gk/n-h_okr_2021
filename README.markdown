# N-H OKR 2021 Slides

This repository contains the slides for GeKo's Network Health OKR presentation
at All Hands 09/15/2021. It's based on a template ahf created (thanks!).

## Building the slides

1. Update the submodules to fetch the `onion-tex` dependency: `git submodule
   update --init --recursive`.

2. Run `make`.
